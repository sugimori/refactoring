# -*- coding: utf-8 -*-
require_relative 'movie'
require_relative 'rental'
require_relative 'customer'

describe Movie do
  subject { Movie.new('test title',100) }
  its(:title) { should be_include 'test title' }
end

describe Rental do
  subject { Rental.new(Movie.new('test title',100),7) }
  its('movie.title') { should be_include 'test title' }
end

describe Customer,"初期化" do
  before { @customer = Customer.new("test name") }

  describe "initialize" do
    subject { @customer }
    its(:name) { should be_include 'test name' }
  end

  describe "statement" do
    context "何も借りてない場合" do
      subject { @customer }
      its(:name) { should be_include 'test name' }
      its(:statement) { should be_include 'Rental Record for test name' }
      its(:statement) { should be_include 'Amount owned is 0' }
      its(:statement) { should be_include 'You earned 0 frequent renter points' }
    end

    context "test title(REGULAR)を7日借りている場合" do
      before { @customer.add_rental(Rental.new(Movie.new('test title',RegularPrice.new),7)) }
      subject { @customer }
      its(:name) { should be_include 'test name' }
      its(:statement) { should be_include 'Rental Record for test name' }
      its(:statement) { should be_include 'Amount owned is 9.5' }
      its(:statement) { should be_include 'You earned 1 frequent renter points' }

    end

    context "2つ借りている場合" do
      before { 
        @customer.add_rental(Rental.new(Movie.new('test title',RegularPrice.new),7))
        @customer.add_rental(Rental.new(Movie.new('test title2',NewReleasePrice.new),3))
      }
      subject { @customer }
      its(:name) { should be_include 'test name' }
      its(:statement) { should be_include 'Rental Record for test name' }
      its(:statement) { should be_include 'Amount owned is 18.5' }
      its(:statement) { should be_include 'You earned 3 frequent renter points' }

    end

  end
end

